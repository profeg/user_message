# User notification application.

## Setup

* clone repo:
`````bash
git clone git@bitbucket.org:profeg/user_message.git
`````
* cd into project's folder, and run:
`````bash
bundle
`````
* create database:
`````bash
rake db:create && rake db:migrate && rake db:seed 
`````
* run test suite:
`````bash
rake test
`````

## Rails console examples
### Create new user:
`````bash
  user_params = {
    first_name: 'Mr',
    last_name: 'Robat',
    email: 'mrrobat@test.com'
  }

  service = UserCreateService.new(user_params)
  service.call   # to execute service
  service.result # to check result
`````
### Create new message draft:
`````bash
  message_params = {
      delivery: 'sms',
      draft: 'Yor win %{username}!'
    }
  service = MessageCreateService.new(message_params)
  service.call   # to execute service
  service.result # to check result
`````
### Send notification to user:
`````bash
  service = NotificationCreateService.new(message.id, user.id)
  service.call   # to execute service
  service.result # to check result
`````
## More examples available at test/services folder.  