class MessageCreateService
  attr_accessor :result, :errors

  def initialize(ops = {})
    @params = ops
  end

  def call
    message = Message.new @params.slice(:delivery, :draft, :code)
    if message.save
      @result = message
      true
    else
      @errors = message.errors
      false
    end
  end
end
