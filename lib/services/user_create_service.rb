class UserCreateService
  attr_accessor :result, :errors

  def initialize(ops = {})
    @params = ops
  end

  def call
    user = User.new @params.slice(:first_name, :last_name, :email, :phone, :email_on, :phone_on)
    if user.save
      @result = user
      true
    else
      @errors = user.errors
      false
    end
  end
end
