class NotificationCreateService
  attr_accessor :result, :errors

  def initialize(message_id, user_id)
    @user = User.find(user_id)
    @message = Message.find(message_id)
  end

  def call
    nt = Notification.new(params)
    if nt.save
      @result = send("send_#{@message.delivery}".to_sym, nt)
      true
    else
      @errors = nt.errors
      false
    end
  end

  private

  def params
    {
      user_id: @user.id,
      message_id: @message.id,
      first_name: @user.first_name,
      last_name: @user.last_name,
      phone: @user.phone,
      email: @user.email,
      message_text: message_text(@message.draft)
    }
  end

  def message_text(text)
    format(text, username: @user.full_name)
  end

  def send_sms(notification)
    {
      phone: notification.phone,
      message: notification.message_text
    }
  end

  def send_email(notification)
    {
      email: notification.email,
      message: notification.message_text
    }
  end
end
