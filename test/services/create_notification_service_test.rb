require 'test_helper'

class CreateNotificationServiceTest < ActiveSupport::TestCase
  test 'the truth' do
    assert true
  end

  test 'send sms message' do
    user = User.first
    message = Message.where(delivery: 'sms').first
    email_ok = {
      phone: user.phone,
      message: "Yor win #{user.full_name}!"
    }
    service = NotificationCreateService.new(message.id, user.id)
    assert service.call
    assert_equal email_ok, service.result
  end

  test 'send email message' do
    user = User.first
    message = Message.where(delivery: 'email').first
    email_ok = {
      email: user.email,
      message: "Yor win #{user.full_name}!"
    }
    service = NotificationCreateService.new(message.id, user.id)
    assert service.call
    assert_equal email_ok, service.result
  end

  test 'fail send email if user deny email send' do
    user = User.first
    message = Message.where(delivery: 'email').first
    error_messages = {
      user: ['User not allowed to receive email']
    }

    user.toggle!(:email_on)

    service = NotificationCreateService.new(message.id, user.id)
    assert_not service.call
    assert_equal error_messages, service.errors.messages
  end

  test 'fail send sms if user deny sms send' do
    user = User.first
    message = Message.where(delivery: 'sms').first
    error_messages = {
      user: ['User not allowed to receive sms']
    }

    user.toggle!(:phone_on)

    service = NotificationCreateService.new(message.id, user.id)
    assert_not service.call
    assert_equal error_messages, service.errors.messages
  end
end
