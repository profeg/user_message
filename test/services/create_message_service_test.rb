require 'test_helper'

class CreateMessageServiceTest < ActiveSupport::TestCase
  test 'the truth' do
    assert true
  end

  test 'create sms message' do
    message_params = {
      delivery: 'sms',
      draft: 'Yor win %{username}!'
    }
    service = MessageCreateService.new(message_params)
    assert service.call
  end

  test 'create email message' do
    message_params = {
      delivery: 'email',
      draft: 'Yor win %{username}!'
    }
    service = MessageCreateService.new(message_params)
    assert service.call
  end

  test 'fail create messages without required fields' do
    error_messages = {
      delivery: ["can't be blank", " is not a valid type"],
      draft: ["can't be blank"]
    }
    service = MessageCreateService.new
    assert_not service.call
    assert_equal error_messages, service.errors.messages
  end
end
