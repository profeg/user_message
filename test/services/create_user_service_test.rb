require 'test_helper'

class CreateUserServiceTest < ActiveSupport::TestCase
  test 'the truth' do
    assert true
  end

  test 'create user with email' do
    user_params = {
      first_name: 'Mr',
      last_name: 'Robat',
      email: 'mrrobat@test.com'
    }
    service = UserCreateService.new(user_params)
    assert service.call
    assert_equal user_params[:first_name], service.result.first_name
    assert_equal user_params[:last_name], service.result.last_name
    assert_equal user_params[:email], service.result.email
    assert_equal true, service.result.email_on
    assert_equal true, service.result.phone_on
  end

  test 'create user with phone' do
    user_params = {
      first_name: 'Mr',
      last_name: 'Robat',
      phone: '+380995100000'
    }
    service = UserCreateService.new(user_params)
    assert service.call
    assert_equal user_params[:first_name], service.result.first_name
    assert_equal user_params[:last_name], service.result.last_name
    assert_equal user_params[:phone], service.result.phone
    assert_equal true, service.result.email_on
    assert_equal true, service.result.phone_on
  end

  test 'fail create user without requiered fields' do
    error_messages = {
      first_name: ['can\'t be blank'],
      last_name: ['can\'t be blank'],
      email_or_phone: ['Phone or email should be present!']
    }
    service = UserCreateService.new
    assert_not service.call
    assert_equal error_messages, service.errors.messages
  end
end
