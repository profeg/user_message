class Message < ActiveRecord::Base
  validates :delivery, :draft, presence: true
  validates :delivery, inclusion: { in: %w[email sms],
    message: "%{value} is not a valid type" }

  def sms?
    delivery == 'sms'
  end

  def email?
    delivery == 'email'
  end
end
