class Notification < ActiveRecord::Base
  validates :user, :message, :code, presence: true
  validates :code, uniqueness: true
  validate :user_notification_permission
  before_validation :generate_code, on: :create

  belongs_to :user
  belongs_to :message

  SMS_NOT_ALLOWED = 'User not allowed to receive sms'.freeze
  EMAIL_NOT_ALLOWED = 'User not allowed to receive email'.freeze

  private

  def generate_code
    self.code = SecureRandom.uuid
  end

  def user_notification_permission
    errors.add(:user, SMS_NOT_ALLOWED) if message.sms? && user.deny_sms?
    errors.add(:user, EMAIL_NOT_ALLOWED) if message.email? && user.deny_emails?
  end
end
