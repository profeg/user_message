class User < ActiveRecord::Base
  validates :first_name, presence: true
  validates :last_name, presence: true
  validate :email_or_phone

  NO_EMAIL_PHONE = 'Phone or email should be present!'.freeze

  def deny_sms?
    phone_on == false
  end

  def deny_emails?
    email_on == false
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  private

  def email_or_phone
    errors.add(:email_or_phone, NO_EMAIL_PHONE) if email.blank? && phone.blank?
  end
end
