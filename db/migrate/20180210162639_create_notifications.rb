class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.belongs_to :user
      t.belongs_to :message
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :email
      t.string :code
      t.text :message_text
      t.timestamps null: false
    end
    add_index :notifications, :code
    add_index :notifications, :user
    add_index :notifications, :message
  end
end
