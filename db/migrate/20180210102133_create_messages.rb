class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :delivery
      t.text :draft

      t.timestamps null: false
    end
    add_index :messages, :type
  end
end
