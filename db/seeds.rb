# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(first_name: 'John', last_name: 'Dow', phone: '380995130000', email: 'johndow@test.com', phone_on: true, email_on: true)
User.create(first_name: 'Sam', last_name: 'Smith', phone: '380995130000', email: 'samsmith@test.com', phone_on: true, email_on: true)
Message.create(delivery: 'sms', draft: 'Yor win %{username}!')
Message.create(delivery: 'email', draft: 'Yor lost, sorry %{username}!')
