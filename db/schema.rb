# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180210162639) do

  create_table "messages", force: :cascade do |t|
    t.string   "delivery"
    t.text     "draft"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "messages", [nil], name: "index_messages_on_type"

  create_table "notifications", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "message_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone"
    t.string   "email"
    t.string   "code"
    t.text     "message_text"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "notifications", ["code"], name: "index_notifications_on_code"
  add_index "notifications", [nil], name: "index_notifications_on_message"
  add_index "notifications", [nil], name: "index_notifications_on_user"

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "phone"
    t.boolean  "email_on",   default: true
    t.boolean  "phone_on",   default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "users", ["email"], name: "index_users_on_email"
  add_index "users", ["first_name"], name: "index_users_on_first_name"
  add_index "users", ["last_name"], name: "index_users_on_last_name"
  add_index "users", ["phone"], name: "index_users_on_phone"

end
